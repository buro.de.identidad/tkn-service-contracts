#!/usr/bin/python
import zipfile
import sys
import string
import shutil
import os
from subprocess import call

"""
argv1 = full or relative path of the odt template file
argv2 = full or relative path of the temporary folder for extracting content, must end in /
argv3 = operation id
argv4 = full or relative location of the odt destination file
argv5 = full name of the customer with underscore separation
argv6 = curp of the customer
argv7 = date with underscore separation
argv8 = tel with no separation
argv9 = email
argv10 = full address with underscore separation

"""
#extracts content to temporal location
try:
    location = sys.argv[2] + sys.argv[3]
    zipContent = zipfile.ZipFile(sys.argv[1])
    zipContent.extractall(location)
    zipContent.close()
    #replace content for correct render
    s = open(location + '/content.xml').read()
    name = sys.argv[5]
    name = name.replace('_', ' ')
    date = sys.argv[7]
    date = date.replace('_' , ' ')
    address = sys.argv[10]
    address = address.replace('_',' ')
    s = s.replace('${NOMCLIEN}', name)
    s = s.replace('${CURP}', sys.argv[6])
    s = s.replace('${DATE}', date)
    s = s.replace('${TEL}', sys.argv[8])
    s = s.replace('${EMAIL}',sys.argv[9])
    s = s.replace('${ADDRESS}',address)
    f = open(location + '/content.xml', 'w')
    f.write(s)
    f.close()
    #zip content again
    shutil.make_archive(sys.argv[4], 'zip', location)
    #delete zip file extension
    os.rename(sys.argv[4] + '.zip', sys.argv[4])
    shutil.rmtree(location)
    #convert document
    call(["unoconv", "-f", "pdf", sys.argv[4]])
    #remove odt file
    os.remove(sys.argv[4])
except:
    print "Unexpected error converting the template file to PDF:", sys.exc_info()[0]
    raise
