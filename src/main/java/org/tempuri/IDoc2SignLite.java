/**
 * IDoc2SignLite.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface IDoc2SignLite extends java.rmi.Remote {
    public java.lang.String load(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador, java.lang.String nombres, java.lang.String apPaterno, java.lang.String apMaterno, java.lang.String RFC, java.lang.String email, java.lang.String documentoBase64, java.lang.Boolean mostrarFirmas, java.lang.String imagenFirma) throws java.rmi.RemoteException;
    public java.lang.String loadMultiple(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador, org.datacontract.autoSigned.FirmanteInfo[] firmantes, org.datacontract.autoSigned.FirmanteInfo firmaEmpresa, java.lang.String documentoBase64, java.lang.Integer mostrarFirmas) throws java.rmi.RemoteException;
    public java.lang.String GETDocFirmado(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException;
    public java.lang.String GETNOM(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException;
    public org.datacontract.autoSigned.CertificadoInfo[] GETCertificado(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String identificador) throws java.rmi.RemoteException;
    public java.lang.String validaNOM(java.lang.String usuarioServicio, java.lang.String passwordServicio, java.lang.String documentoBase64, java.lang.String NOMBase64) throws java.rmi.RemoteException;
}
