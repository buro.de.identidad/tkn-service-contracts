package com.contracts.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import com.contracts.dto.ContractItemDto;
import com.contracts.dto.RequestContractDto;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfImage;
import com.itextpdf.text.pdf.PdfIndirectObject;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;

@Component
public class ContractService {
	private static Logger log = LoggerFactory.getLogger(ContractService.class);

	private String readJsonDocParams(String codeContract) {
	        JsonParser parser = new JsonParser();
	        FileReader paramsJson;
			try {
				paramsJson = new FileReader("/home/config/"+codeContract+".json");//"TMSFID.json");			
				String data =  parser.parse(paramsJson).toString();
				return data;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return null;
	}
	
	/**
	 * Genera el contrato.
	 * @param reqest
	 * @return
	 */
	public String getContract(RequestContractDto reqest) {
		log.info(this.getClass().getName() + ".getContract:" + reqest.getCodeContract());
		try {
			// Obteniendo configuracion de base de datos ...			
			JSONObject dataDB = new JSONObject(readJsonDocParams(reqest.getCodeContract()));			
			// Obteniendo valores e Imprimiendo documento ...
			String docb64pdf = printData(completeItemList(reqest.getCampos(), dataDB.getJSONArray("campos")),reqest.getCodeContract());	
			
			return docb64pdf;
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Obtiene valores especificos del documento y completa con los valores
	 * ingresados.
	 * 
	 * @param impitItemsList
	 * @param camposDB
	 * @return List<ContractItemDto> printItemList
	 * @throws JSONException
	 * @throws JsonSyntaxException
	 */
	private List<ContractItemDto> completeItemList(List<ContractItemDto> impitItemsList, JSONArray camposDB)
			throws JsonSyntaxException, JSONException {
		log.info("INFO: completeItemList: Completando informacion del documento...");
		List<ContractItemDto> printItemList = new ArrayList<>();
		for (int i = 0; i < camposDB.length(); i++) {
			Gson input = new Gson();
			ContractItemDto itemDB = input.fromJson(camposDB.get(i).toString(), ContractItemDto.class);
			for (ContractItemDto imputItem : impitItemsList) {
				if (itemDB.getNombre().equals(imputItem.getNombre())) {
					itemDB.setValor(imputItem.getValor());
					log.info(itemDB.getNombre()+":"+itemDB.getValor());
					printItemList.add(itemDB);
				}else if(!itemDB.getValor().isEmpty()) {
					//add default values.
					log.info(itemDB.getNombre()+":"+itemDB.getValor());
					printItemList.add(itemDB);
				}
			}
		}
		log.info("INFO: Datos a imprimir: >" + printItemList.size());
		return printItemList;
	}

	/**
	 * obtiene la lista de elementos y las imprime en la plantilla
	 * 
	 * @param printItemList
	 * @throws IOException
	 * @throws DocumentException
	 * @throws FileNotFoundException
	 */
	private String printData(List<ContractItemDto> printItemList, String codeContract)
			throws DocumentException, FileNotFoundException, IOException {
		String TEMPLATE = "/home/" + codeContract + ".pdf";
		String TEMPLATEDOC = "src/main/resources/doc/" + codeContract + ".pdf";
		File tempDoc = File.createTempFile("/home/temp/"+codeContract + "_temp", ".pdf");
		tempDoc.getParentFile().mkdirs();
		PdfReader reader = null;
		PdfStamper stamper = null;
		try {
			// carga template del entorno docker. Productivo.
			reader = new PdfReader(TEMPLATE);
			stamper = new PdfStamper(reader, new FileOutputStream(tempDoc));
		} catch (IOException e) {
			// carga template del entorno local Para pruebas.
			log.error("Error: error al cargar el archivo del entorno docker home" + e);
			try {
				reader = new PdfReader(TEMPLATEDOC);
				stamper = new PdfStamper(reader, new FileOutputStream(tempDoc));
			} catch (IOException e1) {
				log.error("Error: Error al cargar el archivo local" + e1);
			}
		}

		for (ContractItemDto printItem : printItemList) {
			PdfContentByte pag = stamper.getOverContent(printItem.getPag());
			if (printItem.isImg()) {
				if(printItem.getValor()!=null&&!printItem.getValor().isEmpty()) {
					log.info(">Insertando imagen..");				
					Image image = createImage(printItem.getValor());
					PdfImage stream = new PdfImage(image, "", null);
					stream.put(new PdfName("ITXT_SpecialId"), new PdfName("123456789"));
					PdfIndirectObject ref = stamper.getWriter().addToBody(stream);
					image.setDirectReference(ref.getIndirectReference());
					image.scaleAbsolute(40f, 40f);
					image.setAbsolutePosition(printItem.getX(), printItem.getY());
					pag.addImage(image);
					log.info(">imagen insertada.");
				}
			} else {
				log.info(">Insertando: "+printItem.getNombre()+":"+printItem.getValor());
				ColumnText.showTextAligned(pag, printItem.getAlignment(),
						new Phrase(printItem.getValor(),
								new Font(FontFamily.valueOf(printItem.getFontStyle()), printItem.getFontSize())),
						printItem.getX(), printItem.getY(), 0);
			}			
		}
		stamper.close();
		reader.close();
		log.info(">PDF Generado.");
		return Base64.getEncoder().encodeToString(fileToByte(tempDoc));
	}
	
	/**
	 * Crea un archivo de huell apar firmado.
	 * @param huellaFinal
	 * @return
	 */
	private Image createImage(String huellaFinal) {
		byte[] imageByte;   	
		imageByte = Base64Utils.decodeFromString(huellaFinal);
		File rutaHuella = new File("/home/fingerToFirma.png");		
		if(rutaHuella.exists()) {			
			rutaHuella.delete();
			rutaHuella = new File("/home/fingerToFirma.png");
		}		
		InputStream in = new ByteArrayInputStream(imageByte);
		BufferedImage bImageFromConvert;
		try {
			bImageFromConvert = ImageIO.read(in);		
			ImageIO.write(bImageFromConvert, "png", rutaHuella );
			return Image.getInstance("/home/fingerToFirma.png");		
		} catch (Exception e) {
			log.info("ERROR:"+e.getMessage());
			e.printStackTrace();
		}
		return null;		
	}
	
	/**
	 * Metodo para ocnvertir File a Byte[].
	 * 
	 * @param file
	 * @return byte[]
	 * @throws IOException
	 */
	@SuppressWarnings("resource")
	private byte[] fileToByte(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			throw new IOException("File is too large" + file.getName());
		}
		byte[] bytes = new byte[(int) length];
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}
		is.close();
		return bytes;
	}

	/**
	 * Metodo para convertir String a file
	 * 
	 * @param key
	 * @param encoded
	 * @return File
	 * @throws IOException
	 */
	private File base64ToFile(String extension, String encoded) throws IOException {
		if (encoded == null || encoded.isEmpty()) {
			return null;
		}
		File convFile = File.createTempFile("tempImg", extension);
		convFile.getParentFile().mkdirs();
		byte[] decodeImg = Base64.getDecoder().decode(encoded.getBytes(StandardCharsets.UTF_8));		
		FileUtils.writeByteArrayToFile(convFile, decodeImg);
		return convFile;
	}

}