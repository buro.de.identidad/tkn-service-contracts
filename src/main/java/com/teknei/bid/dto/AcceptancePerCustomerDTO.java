package com.teknei.bid.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class AcceptancePerCustomerDTO implements Serializable {

    private String signCode;
    private String signDesc;
    private Boolean required;
    private Boolean accepted;
    private Long idClie;
    private String usernameRequesting;

}