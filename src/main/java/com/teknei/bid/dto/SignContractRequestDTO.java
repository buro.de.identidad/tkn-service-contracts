package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SignContractRequestDTO implements Serializable {

    private Long operationId;
    private String base64Finger;
    private byte[] contractSigned;
    private String contentType;
    private String username;
    private String hash;

}