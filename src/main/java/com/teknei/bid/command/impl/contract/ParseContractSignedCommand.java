package com.teknei.bid.command.impl.contract;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Base64;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;
import org.tempuri.Doc2SignLite;
import org.tempuri.Doc2SignLiteLocator;
import org.tempuri.IDoc2SignLite;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.dto.SignContractRequestDTO;
import com.teknei.bid.persistence.entities.BidClie;
import com.teknei.bid.persistence.entities.BidClieMail;
import com.teknei.bid.persistence.repository.BidClieRepository;
import com.teknei.bid.persistence.repository.BidMailRepository;
import com.teknei.bid.service.ContractSignService;
import com.teknei.bid.service.ContractTasService;

@Component
public class ParseContractSignedCommand implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseContractSignedCommand.class);
    @Autowired
    private ContractSignService contractSignService;
    @Autowired
    private ContractTasService contractTasService;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidMailRepository bidMailRepository;
       
    private Integer noRetry = 3; //TODO get this from config
    
    private IDoc2SignLite service;

    @Value("${tkn.cert.psc-sign-active}")
    private boolean pscSignActive = true;
    
    @Value("${karalundiCertV3.user}")
    private String user;
    
    @Value("${karalundiCertV3.pass}")
    private String pass;
    		
	@PostConstruct
	private void postConstruct() {
		try {
			Doc2SignLiteLocator ser = new Doc2SignLiteLocator();
			service = ser.getBasicHttpBinding_IDoc2SignLite();
		} catch (ServiceException e) {
			e.printStackTrace();
		}	
	}

	

    @Override
    public CommandResponse execute(CommandRequest request) {
    	// CREATE CONTRACT 
    	String contractBase64 = "";
    	log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        SignContractRequestDTO signContractRequestDTO = new SignContractRequestDTO();
        signContractRequestDTO.setBase64Finger(request.getData());
        signContractRequestDTO.setContentType(request.getData2());
        signContractRequestDTO.setOperationId(request.getId());
        signContractRequestDTO.setHash(request.getScanId());
        try 
        {
        	// firma biometrica
            byte[] contractSigned = contractSignService.signContract(signContractRequestDTO);
            if (contractSigned == null) {
                for (int i = 0; i < noRetry; i++) {
                    contractSigned = contractSignService.signContract(signContractRequestDTO);
                    if (contractSigned != null) {
                        break;
                    }
                }
            }
            if (contractSigned == null) {
                response.setStatus(Status.CONTRACT_ERROR);
            } else {
                response.setStatus(Status.CONTRACT_OK);
              //TODO guardar en tas
                log.info("[tkn-service-contracts] :: SAVE ON TAS ");
                contractTasService.addContactPrefilledWithCertToTas(contractSigned, request.getId());
            }
            
            log.info("[tkn-service-contracts] :: Generacion de contrato : "+response.getStatus());

            
            BidClie bidClie = bidClieRepository.findOne(request.getId());
            BidClieMail bidClieMail = bidMailRepository.findTopByIdClie(request.getId());
            String nombres = bidClie.getNomClie();
            String apPaterno = bidClie.getApePate();
            String apMaterno = bidClie.getApeMate();
            String rfc = bidClie.getRfc();
            String email = bidClieMail.getEmai();
            boolean mostrarFirmas=false;
            String imagenFirma="";
            String identificador =""+signContractRequestDTO.getOperationId();
			String usuarioServicio = user;
			String passwordServicio = pass;
            
            contractBase64 =  Base64Utils.encodeToString(contractSigned);
            
            log.info("Nombres>"+nombres);
            log.info("ApPaterno>"+apPaterno);
            log.info("ApMaterno>"+apMaterno);
            log.info("RFC>"+rfc);
            log.info("mail>"+email);
            log.info("Identificador>"+identificador);
            log.info("length : "+(contractBase64.length()));
            

			String resultado = service.load(usuarioServicio, passwordServicio, identificador, nombres, apPaterno, apMaterno,
					rfc, email, contractBase64, mostrarFirmas, imagenFirma);
			log.info("INFO: Resultado  cargaCertificado : " + resultado);

			String docFirmado = service.GETDocFirmado(usuarioServicio, passwordServicio, identificador);
			log.info("INFO: Resultado  docFirmado : "+docFirmado.length());			
            
//            writeInFile("pdf.encode",docFirmado);    
            String docNom151 = service.GETNOM(usuarioServicio, passwordServicio, identificador);
                        
            log.info("Resultado NOM : "+docNom151.length());
//            writeInFile("data.encode",docNom151);
           //------------------------------------
//            se modifica metodo de transformado de cadena base 64 
           //------------------------------------
            
            contractSigned =  Base64.getDecoder().decode(new String(docFirmado).getBytes("UTF-8"));
           // contractSigned = (byte[] )Base64Utils.decode(docFirmado.getBytes());
            
//            writeInFile("pdf",contractSigned);
            
            log.info("docFirmado decode Base 64 [docFirmadoDecode]");
            log.info("docFirmadoDecode Transformado a Bytes [contractSigned]");
            
            // pasar de string a bytes 
            byte[] nom151 = Base64.getDecoder().decode(new String(docNom151).getBytes("UTF-8"));
            
//            byte[] nom151= (byte[] )Base64Utils.decode(docNom151.getBytes());
            
//            writeInFile("data",contractSigned);
            
            log.info("docNom151 decode Base 64[nom151Decode]");
                         
            log.info("docFirmadoDecode Transformado a Bytes [nom151]");
              if (docNom151 != null && docNom151.length()>0) {
                  log.info("NOM151 OK");
                  try {
                      contractTasService.addContactNom151EvidencetToTas(nom151, signContractRequestDTO.getOperationId());
                      log.info("Added NOM151 to TAS services");
                  } catch (Exception e) {
                      log.error("Error adding NOM151 TAS service");
                  }
              } else {
                  log.info("No NOM151, null");
              }    
            response.setSignedContract(contractSigned);
            contractBase64 = "";
        } catch (Exception e) {
            log.error("Error signing contract for request: {} with message: {}", request.getId(), e.getMessage());
            response.setStatus(Status.CONTRACT_OK);
//          saveFileIfFail(contractBase64);
        }
        return response;
    }
    
    
    private void saveFileIfFail(String contractoNoFirmado) {
    	if(!contractoNoFirmado.isEmpty()) {
    		log.info(">Guardando pdf..");
    		writeInFile("pdf",contractoNoFirmado);
    	} 	
    }
    
    
    private void writeInFile(String ext, String body)
    {
    	String fileName=""+(new Date()).getTime()+"."+ext;
    	String ruta="/home/";
    	{	
    		try {
    			File file = new File(ruta+fileName);
    			log.info(file.getAbsolutePath());
    			FileWriter fileWriter = new FileWriter(file);
    			fileWriter.write(body);
    			fileWriter.flush();
    			fileWriter.close();
    		} catch (IOException e) {
    			e.printStackTrace();
    		}
    	}
    }
    public void writeInFile(String  ext,byte[] strToBytes)
    {
    	String fileName=""+(new Date()).getTime()+"."+ext;
    	String ruta="/home/";
        FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(ruta+fileName);
			outputStream.write(strToBytes);
	        outputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
    }

}
