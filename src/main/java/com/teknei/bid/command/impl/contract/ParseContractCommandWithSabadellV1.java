package com.teknei.bid.command.impl.contract;
import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.*;
import com.teknei.bid.persistence.repository.*;
import com.teknei.bid.service.ContractExstreamService;
import com.teknei.bid.service.ContractOTTService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
@Component
public class ParseContractCommandWithSabadellV1 implements Command {

    private static final Logger log = LoggerFactory.getLogger(ParseContractCommandWithSabadellV1.class);

    @Autowired
    private ContractExstreamService contractExstreamService;
    @Autowired
    private BidDireNestRepository bidDireNestRepository;
    @Autowired
    private BidClieRepository bidClieRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;
    @Autowired
    private BidDireRepository bidDireRepository;
    @Autowired
    private BidMailRepository bidMailRepository;
    @Autowired
    private BidTelRepository bidTelRepository;
    @Autowired
    private BidIfeRepository bidIfeRepository;
    @Autowired
    private ContractOTTService contractOTTService;
    @Autowired
    private BidCliePasaRepository bidCliePasaRepository;
    @Autowired
    private BidClieCertRepository bidClieCertRepository;
    @Autowired
    private BidClieContRepository bidClieContRepository;
    @Value("${tkn.contract.provider}")
    private String contractProvider;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy");
    private Integer noRetry = 3; //TODO get this for config

    @Override
    public CommandResponse execute(CommandRequest request) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".execute ");
    	//log.info("[tkn-service-contracts]  : com.teknei.bid.command.impl.contract.ParseContractCommandWithSabadellV1.execute");
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        String address = "";
        try {
            BidClieDirePK direPK = new BidClieDirePK();
            direPK.setIdDire(request.getId());
            direPK.setIdClie(request.getId());
            BidClieDire clieDire = bidDireRepository.findOne(direPK);
            if (clieDire == null) {
                BidClieDireNestPK bidClieDireNestPK = new BidClieDireNestPK();
                bidClieDireNestPK.setIdClie(request.getId());
                bidClieDireNestPK.setIdDire(request.getId());
                BidClieDireNest bidClieDireNest = bidDireNestRepository.findOne(bidClieDireNestPK);
                address = bidClieDireNest.getDirNestDos();
            } else {
                address = new StringBuilder(clieDire.getCall()).append(" ").append(clieDire.getCol()).append(" ").append(clieDire.getCp()).toString();
            }

            BidClieIfeInePK ifeInePK = new BidClieIfeInePK();
            ifeInePK.setIdIfe(request.getId());
            ifeInePK.setIdClie(request.getId());
            BidClieIfeIne ifeIne = null;
            BidCliePasa bidCliePasa = null;
            String ocr = "NA";
            try {
                ifeIne = bidIfeRepository.findOne(ifeInePK);
                if (ifeIne != null) {
                    ocr = ifeIne.getOcr();
                } else {
                    BidCliePasaPK pasaPK = new BidCliePasaPK();
                    pasaPK.setIdPasa(request.getId());
                    pasaPK.setIdClie(request.getId());
                    BidCliePasa pasa = bidCliePasaRepository.findOne(pasaPK);
                    if (pasa != null) {
                        ocr = pasa.getNumPasa();
                    }
                }
            } catch (Exception e) {
                log.error("Error finding OCR value: {}", e.getMessage());
            }
            BidClie bidClie = bidClieRepository.findOne(request.getId());
            BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(request.getId());
            String name = bidClie.getNomClie();
            String lastName = new StringBuilder(bidClie.getApeMate()).append(" ").append(bidClie.getApeMate()).toString();
            String curp = bidClieCurp.getCurp();
            String dateStr = sdf.format(new Date());
            BidClieMail bidClieMail = bidMailRepository.findTopByIdClie(request.getId());
            BidTel bidTel = bidTelRepository.findTopByIdClie(request.getId());
            String mail = "example@mail.com";
            String tel = "1122334455";
            if (bidClieMail != null) {
                mail = bidClieMail.getEmai();
            }
            if (bidTel != null) {
                tel = bidTel.getTele();
            }
            byte[] contract = null;
            if (contractProvider.toUpperCase().contains("ODT")) 
            {
            	//log.info("[tkn-service-contracts]  > 0 : SE MODIFICA EL CLIENTE A 14 LBLANCAS+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                BidClieCont clieCont = bidClieContRepository.findByIdClie(request.getId());
                System.out.println("lblancas > 1 : "+request.getId());
                System.out.println("lblancas > 2 : "+clieCont.toString());
                clieCont = modify(clieCont);
                System.out.println("lblancas > 3 : ");
                contract = contractOTTService.generateContractSabadellWithArgumentsAndSerial(clieCont, 0l, 0l, "NA", "NA", "NA", "NA");
                System.out.println("lblancas > 4 : ");
                if(contract == null)
                {
                	System.out.println("lblancas > 5 : ");
                    for (int i = 0; i < noRetry; i++) 
                    {
                    	System.out.println("lblancas > 6 : ");
                        contract = contractOTTService.generateContractSabadellWithArgumentsAndSerial(clieCont, 0l, 0l, "NA", "NA", "NA", "NA");
                        if(contract != null)
                        {
                        	System.out.println("lblancas > 7 : ");
                            break;
                        }
                    }
                }
            } 
            else 
            {
            	/*log.info("[tkn-service-contracts] > 10 : "
            			+ "name      : "+name
            			+ "lastName  :"+lastName
            			+ "curp       :"+curp
            			+ "ocr        :"+ocr
            			+ "address    :"+address
            			+ "dateStr    :"+dateStr
            			+ "mail       :"+mail
            			+ "telefono   :"+tel);
            			*/
                contract = contractExstreamService.generateContract(name, lastName, curp, ocr, address, dateStr, mail, tel, tel);
            }
            if(contract == null){
                response.setStatus(Status.CONTRACT_EXSTREAM_ERROR);
            }else{
                response.setStatus(Status.CONTRACT_EXSTREAM_OK);
            }
            response.setContract(contract);
            System.out.println("lblancas > 101 : "
        			+ "response: "+response.imprime());
            
            
        } catch (Exception e) {
            log.error("Erorr in parseContractCommand With serial TS for request: {} with message: {}", request, e.getMessage());
            e.printStackTrace();
            response.setStatus(Status.CONTRACT_EXSTREAM_ERROR);
        }
        return response;
    }

    private BidClieCont modify(BidClieCont source) {
    	//log.info("[tkn-service-contracts]:: "+this.getClass().getName()+".modify ");
    	//log.info("[tkn-service-contracts]: com.teknei.bid.command.impl.contract.ParseContractCommandWithSabadellV1.modify");
        Class<?> clazz = source.getClass();
        Class<?> clazzString = String.class;
        try {
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields) {
                Class<?> clazzType = f.getType();
                if (clazzType.isAssignableFrom(clazzString)) {
                    f.setAccessible(true);
                    String previousValue = (String) f.get(source);
                    if (previousValue == null) {
                        previousValue = "NA";
                    }
                    previousValue = previousValue.toString().replace(" ", "_");
                    f.set(source, previousValue);
                }
            }
        } catch (Exception e) {
            log.error("Error assigning reflection value: {}", e.getMessage());
        }
        return source;
    }


}
