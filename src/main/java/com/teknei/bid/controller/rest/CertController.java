package com.teknei.bid.controller.rest;

import com.teknei.bid.dto.CertCreatedDTO;
import com.teknei.bid.dto.CertKey;
import com.teknei.bid.service.CertGenerationService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/cert")
public class CertController {

    @Autowired
    private CertGenerationService certGenerationService;
    private static final Logger log = LoggerFactory.getLogger(CertController.class);

    @RequestMapping(value = "/cert/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getCert(@PathVariable Long id) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getCert ");
        JSONObject jsonObject = new JSONObject();
        String serial = certGenerationService.findSerialByIdClient(id);
        if (serial == null) {
            jsonObject.put("serial", "NA");
            return new ResponseEntity<>(jsonObject.toString(), HttpStatus.NOT_FOUND);
        } else {
            jsonObject.put("serial", serial);
        }
        return new ResponseEntity<>(jsonObject.toString(), HttpStatus.OK);
    }

    @RequestMapping(value = "/cert", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> generateCert(@RequestBody CertKey certKey) 
    {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".generateCert ");
        int retryNo = certGenerationService.getNoRetry();
        CertCreatedDTO certCreatedDTO = null;
        for (int i = 0; i < retryNo; i++) {
            certCreatedDTO = certGenerationService.generateCert(certKey);
            certGenerationService.saveOrUpdateCert(Long.parseLong(certKey.getCustomerId()), certCreatedDTO.getSerial(), certKey.getUsernameRequesting(), certCreatedDTO.getUsername(), String.valueOf(certCreatedDTO.getExitCode()));
            if (certCreatedDTO.getExitCode() == 0 || certCreatedDTO.getExitCode() == 409) {
                log.info("Serial created or existent:  {}  :::::::: where 0=new and 409=created before", certCreatedDTO.getExitCode());
                return getSerial(Long.parseLong(certKey.getCustomerId()), certCreatedDTO.getUsername());
            }
        }
        if (certCreatedDTO != null && certCreatedDTO.getExitCode() == 403) {
            return new ResponseEntity<>("BAD_CREDENTIALS", HttpStatus.FORBIDDEN);
        } else {
            log.info("Error reached for cert generation not recognized: {}", certCreatedDTO);
            return new ResponseEntity<>("PROVIDER_ERROR", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    private ResponseEntity<String> getSerial(Long idCustomer, String username) {
    	//log.info("[tkn-service-contracts] :: "+this.getClass().getName()+".getSerial ");
        try {
            String serial = certGenerationService.findSerial(idCustomer, username);
            return new ResponseEntity<>(serial, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            log.error("No serial found in db for customer that already have a valid certificate!");
            return new ResponseEntity<>("CERT_ALREADY_GENERATED_BUT_NOT_IN_DB", HttpStatus.CONFLICT);
        }
    }
}
